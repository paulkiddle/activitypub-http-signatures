import { expect, test } from '@jest/globals';
import parser, { FetchLiteOptions, Sha256Signature, Sha256Signer, SignedFetch, UnkownAlgorithmError } from './main.js';
import keys from './keys.fixture.js';

test('sign & verify', () => {
	const publicKeyId = 'https://example.com/';

	const signer = new Sha256Signer({
		publicKeyId,
		privateKey: keys.private
	});
	const req = {
		url: 'https://request.example/etc?a=b',
		method: 'POST',
		headers: {}
	};
	const reqPost = {
		url: 'https://request.example/etc?a=b',
		method: 'POST',
		headers: {
			digest: 'digest-value'
		}
	};

	const generatedHeaders = signer.generateHeaders(req);
	const generatedHeaders2 = signer.generateHeaders({ url: req.url, method: req.method });
	const generatedHeadersPost = signer.generateHeaders(reqPost);

	const signatureHeader = signer.sign(req);
	const signatureHeaderPost = signer.sign(reqPost);

	const headers = {
		...req.headers,
		signature: signatureHeader
	}
	const headersPost = {
		...reqPost.headers,
		signature: signatureHeaderPost
	}

	expect(generatedHeaders).toEqual(headers);
	expect(generatedHeaders2).toEqual(headers);
	expect(generatedHeadersPost).toEqual(headersPost);

	const incoming = {
		url: '/etc?a=b',
		method: 'POST',
		headers
	};

	const signature = parser.parse(incoming);

	expect(signature.keyId).toBe(publicKeyId);

	expect(signature.verify(keys.public)).toBe(true);
});

test('signed fetch', async () => {
	const publicKeyId = 'https://example.com/';

	async function fetch(url: string, options?: FetchLiteOptions<string>) { return [url, options] as const };

	const signOptions = { publicKeyId, privateKey: keys.private };
	const signedFetch = SignedFetch.sha256(fetch, signOptions);

	const url = 'https://request.example/etc?a=b';

	const signer = new Sha256Signer(signOptions);
	const generatedHeaders = signer.generateHeaders({ url, method: 'POST' });
	const generatedHeaders2 = signer.generateHeaders({ url, method: 'GET' });

	expect(await signedFetch(url, { method: 'POST' })).toEqual([url, { method: 'POST', headers: generatedHeaders }]);
	expect(await signedFetch(url)).toEqual([url, { headers: generatedHeaders2 }]);
});

test('Parse returns null if no signature header', ()=>{
	const req = {
		url: '/',
		method: 'GET',
		headers: {
		}
	};

	expect(parser.parse(req)).toBe(null);
});

test('Parse throws on unexpected algo', ()=>{
	const req = {
		url: '/',
		method: 'GET',
		headers: {
			signature: 'signature="\\IA==",algorithm=unknown'
		}
	};

	expect(()=>parser.parse(req)).toThrow(UnkownAlgorithmError);
});

test('Parse defaults to sha256 if no algo present', ()=>{
	const req = {
		url: '/',
		method: 'GET',
		headers: {
			signature: 'signature="IA=="'
		}
	};

	expect(parser.parse(req)).toBeInstanceOf(Sha256Signature);
});
