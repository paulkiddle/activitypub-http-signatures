/**
 * Activitypub HTTP Signatures
 * Based on [HTTP Signatures draft 08](https://datatracker.ietf.org/doc/html/draft-cavage-http-signatures-08)
 * @module activitypub-http-signatures
 */

import assert from 'assert';
import crypto from 'crypto';
import { IncomingHttpHeaders } from 'http';


// token definitions from definitions in rfc7230 and rfc7235
const token	= String.raw`[!#$%&'\*+\-\.\^_\`\|~0-9a-z]+`;	// Key or value
const qdtext	= String.raw`[^"\\\x7F]`;	// Characters that don't need escaping
const quotedPair	= String.raw`\\[\t \x21-\x7E\x80-\xFF]`;	// Escaped characters
const quotedString	= `(?:${qdtext}|${quotedPair})*`;
const fieldMatch	= new RegExp(String.raw`(?<=^|,\s*)(${token})\s*=\s*(?:(${token})|"(${quotedString})")(?=,|$)`, 'ig');
const parseSigFields	= (str: string) => Object.fromEntries(
	Array.from(
		str.matchAll(fieldMatch)
	).map(
		// capture groups: 1=fieldname, 2=field value if unquoted, 3=field value if quoted
		v=>[
			v[1],
			v[2] ?? v[3].replace(
				/\\./g,
				c=>c[1]

			)
		]
	)
);

export function parseSignatureHeader(header: string){
	const fields = parseSigFields(header);
	return {
		fields,
		headers: (fields.headers ?? 'date').split(/\s+/),
		signature: Buffer.from(fields.signature, 'base64'),
		keyId: fields.keyId,
		algorithm: fields.algorithm ?? 'rsa-sha256'
	}
}

const defaultHeaderNames = ['(request-target)', 'host', 'date', 'digest'];

type Request = {
	method: string,
	headers: Record<string, string> | IncomingHttpHeaders
}

type SpecialHeadersOptions = Request & { target: string }

/**
 * @private
 * Generate the string to be signed for the signature header
 */
function getSignString(headers: Headers, headerNames: string[]) {
	return headerNames.map(header => `${header.toLowerCase()}: ${headers[header]}`).join('\n');
}

function getSpecialHeaders({ target, method, headers }: SpecialHeadersOptions) {
	const requestTarget = `${method.toLowerCase()} ${target}`;

	return {
		...headers,
		'(request-target)': requestTarget
	};
}

type SignerOptions = {
	publicKeyId: string,
	privateKey: string | Buffer,
	headerNames?: string[] | null
}

type Headers = Record<string, string> | IncomingHttpHeaders;

type RequestProperties<H extends Headers = Headers> = {
	method?: string
	url?: string
	headers: H
}

export type GenerateHeadersOptions = {
	url: string,
	method: string,
	headers?: Record<string, string> | null
}

export interface Signer {
	generateHeaders(options: GenerateHeadersOptions): Record<string, string>
}

export class Sha256Signer implements Signer {
	#publicKeyId;
	#privateKey;
	#headerNames;

	/**
	 * Class for signing a request and returning the signature header
	 * @param {object}	options	Config options
	 * @param {string}	options.publicKeyId	URI for public key that must be used for verification
	 * @param {string}	options.privateKey	Private key to use for signing
	 * @param {string[]}	options.headerNames	Names of headers to use in generating signature
	 */
	constructor({ publicKeyId, privateKey, headerNames }: SignerOptions) {
		this.#publicKeyId = publicKeyId;
		this.#privateKey = privateKey;
		this.#headerNames = headerNames ?? defaultHeaderNames;
	}

	/**
	 * Generate the signature header for an outgoing message
	 * @param	{object}	reqOptions	Request options
	 * @param	{string}	reqOptions.url	The full URL of the request to sign
	 * @param	{string}	reqOptions.method	Method of the request
	 * @param	{object}	reqOptions.headers	Dict of headers used in the request
	 * @returns	{string}	Value for the signature header
	 */
	sign({ url, method, headers }: RequestProperties) {
		assert(url);
		assert(method);
	
		const { host, pathname, search } = new URL(url);
		const target = `${pathname}${search}`;
		headers.date ??= new Date().toUTCString();
		headers.host ??= host;

		const specialHeaders = getSpecialHeaders({ target, method, headers });

		const headerNames = this.#headerNames.filter(h => h in specialHeaders);

		const stringToSign = getSignString(specialHeaders, headerNames);

		const signature = this.#signSha256(this.#privateKey, stringToSign).toString('base64');

		return `keyId="${this.#publicKeyId}",headers="${headerNames.join(' ')}",signature="${signature.replace(/"/g, '\\"')}",algorithm="rsa-sha256"`;
	}

	generateHeaders({ url, method, headers }: GenerateHeadersOptions): Record<string, string> {
		const headersCopy = headers ? { ...headers } : {};
		const signature = this.sign({ url, method, headers: headersCopy });

		return {
			...headersCopy,
			signature
		}
	}

	/**
	 * @private
	 * Sign a string with a private key using sha256 alg
	 * @param {string} privateKey Private key
	 * @param {string} stringToSign String to sign
	 * @returns {Buffer} Signature buffer
	 */
	#signSha256(privateKey: string | Buffer, stringToSign: string) {
		const signer = crypto.createSign('sha256');
		signer.update(stringToSign);
		const signature = signer.sign(privateKey);
		signer.end();
		return signature;
	}
}

export type FetchLite<B, R> = (url: string, options?: FetchLiteOptions<B>) => Promise<R>
export type FetchLiteOptions<B> = {
	method?: string,
	headers?: Record<string, string>
	body?: B
}

export const SignedFetch = {
	generic<B, R>(fetch: FetchLite<B, R>, signer: Signer): FetchLite<B, R> {
		return function signedFetch(url: string, options?: FetchLiteOptions<B>){
			const headers = signer.generateHeaders({ url, method: options?.method ?? 'GET', headers: options?.headers });
			return fetch(url, {
				...options,
				headers
			})
		}
	},

	sha256<B, R>(fetch: FetchLite<B, R>, o: SignerOptions): FetchLite<B, R> {
		return this.generic(fetch, new Sha256Signer(o));
	}
}

type SignatureFactoryOptions = {
	signature: Buffer,
	string: string,
	keyId: string
}

/**
 * Incoming request parser and Signature factory.
 * If you wish to support more signature types you can extend this class
 * and overide getSignatureClass.
 */
export class Parser {
	/**
	 * Parse an incomming request's http signature header
	 * @param	{object}	reqOptions	Request options
	 * @param	{string}	reqOptions.url	The pathname (and query string) of the request URL
	 * @param	{string}	reqOptions.method	Method of the request
	 * @param	{object}	reqOptions.headers	Dict of headers used in the request
	 * @returns {Signature|null} Object representing the signature, or null if no signature header is present
	 * @throws	{UnkownAlgorithmError}	If the algorithm used isn't one we know how to verify
	 */
	parse<H extends Headers>({ headers, method, url }: RequestProperties<H>): H extends { signature: string } ? Signature : (Signature | null){
		assert(url, 'No value for `url` was provided');
		assert(method, 'No value for `method` was provided');

		if(typeof headers.signature !== 'string') {
			return null as (Signature & null);
		}

		const { algorithm, signature, headers: headerNames, keyId } = parseSignatureHeader(headers.signature);
		const specialHeaders = getSpecialHeaders({ target: url, method, headers })
		const signString = getSignString(specialHeaders, headerNames);

		return this.getSignatureClass(algorithm, { signature, string: signString, keyId });
	}

	/**
	 * Construct the signature class for a given algorithm.
	 * Override this method if you want to support additional
	 * algorithms.
	 * @param	{string}	algorithm The algorithm used by the signed request
	 * @param	{object}	options
	 * @param	{Buffer}	options.signature	The signature as a buffer
	 * @param	{string}	options.string	The string that was signed
	 * @param	{string}	options.keyId	The ID of the public key to be used for verification
	 * @returns	{Signature}
	 * @throws	{UnkownAlgorithmError}	If an unknown algorithm was used
	 */
	getSignatureClass(algorithm: string, { signature, string, keyId }: SignatureFactoryOptions) {
		if(algorithm === 'rsa-sha256') {
			return new Sha256Signature({ signature, string, keyId });
		} else {
			throw new UnkownAlgorithmError(`Don't know how to verify ${algorithm} signatures.`);
		}
	}
}

export class UnkownAlgorithmError extends Error {}

export abstract class Signature {
	#keyId;

	constructor(keyId: string) {
		this.#keyId = keyId;
	}

	get keyId(){
		return this.#keyId;
	}

	abstract verify(key: string | Buffer): boolean
}

export class Sha256Signature extends Signature {
	#signature;
	#string;

	/**
	 * Class representing the HTTP signature
	 * @param	{object}	options
	 * @param	{Buffer}	options.signature	The signature as a buffer
	 * @param	{string}	options.string	The string that was signed
	 * @param	{string}	options.keyId	The ID of the public key to be used for verification
	 */
	constructor({ signature, string, keyId }: SignatureFactoryOptions) {
		super(keyId);
		this.#signature = signature;
		this.#string = string;
	}

	/**
	 * @property {string} keyId The ID of the public key that can verify the signature
	 */

	/**
	 * Verify the signature using a public key
	 * @param	{string} key The public key matching the signature's keyId
	 * @returns	{boolean}
	 */
	verify(key: string | Buffer) {
		const signature = this.#signature;
		const signedString = this.#string;
		const verifier = crypto.createVerify('sha256');
		verifier.write(signedString);
		verifier.end();

		return verifier.verify(key, signature);
	}
}

/**
 * Default export: new instance of Parser class
 */
export default new Parser;
