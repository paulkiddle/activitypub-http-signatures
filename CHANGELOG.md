# Changes

## v2.5.0

- Added `digest` to the list of default header names for signing
- A header name is now omitted from signature generation if it's not in the request headers dict

## v2.4.0

- Added a new SignedFetch feature

## v2.3.0

- Verify method returns `null` instead of throwing an exception if there's no signature header

### v2.3.1

- Actually build the package before publishing

## v2.2.0

 - Export a Typescript interface for `Signer` classes
 - Export a Typescript interface for the `GenerateHeadersOptions` object

## v2.1.1

- Removed Node engine version requirement

## v2.1.0

- Added typescript definitions
- Added new `generateHeaders` method to use instead of`sign`
